<?php


/**
 * Implements hook_init().
 *
 * Includes the two module-specific include files and calls their init functions
 * if their respective module is enabled.
 *
 * @see acquia_search_multi_subs_apachesolr_init()
 * @see acquia_search_multi_subs_searchapi_init()
 */
function acquia_search_config_init() {
  if (module_exists('search_api_acquia') && module_exists('search_api') && module_exists('search_api_solr')) {
    include_once dirname(__FILE__) . '/lib/Drupal/SearchApiSolr/acquia_search_config.searchapisolr.inc';
  }
  if (module_exists('acquia_search')) {
    include_once dirname(__FILE__) . '/lib/Drupal/Apachesolr/acquia_search_config.apachesolr.inc';
  }
}

/**
 * Take care of the autoloading.
 *
 * @return bool
 *   return true
 */
function acquia_search_config_load() {
  if (!class_exists('SearchServiceClient', true)) {
    if (module_exists('composer_manager') && function_exists('composer_manager_register_autoloader')) {
      composer_manager_register_autoloader();
    }
    elseif (file_exists(__DIR__ . '/vendor/autoload.php')) {
      require_once __DIR__ . '/vendor/autoload.php';
    }
    else {
      drupal_set_message('Could not find the Acquia Search Service Class. Please use composer to grab the Acquia Search Client PHP library or use composer_manager to help you.', 'error');
      return FALSE;
    }
  }
  return TRUE;
}

/**
 * Implements hook_menu().
 */
function acquia_search_config_menu() {
  $items = array();

  if (module_exists('search_api_acquia') && module_exists('search_api') && module_exists('search_api_solr')) {

  }

  if (module_exists('acquia_search')) {
    $items['admin/config/search/apachesolr/settings/%apachesolr_environment/files'] = array(
      'title'              => 'Configuration Files',
      'page callback'      => 'drupal_get_form',
      'page arguments'     => array('apachesolr_environment_configuration_files_form', 5),
      'description'        => 'Edit Acquia Search Configuration files',
      'access arguments'   => array('administer search'),
      'weight'             => 10,
      'file'               => 'lib/Drupal/Apachesolr/acquia_search_config.apachesolr.inc',
      'type'               => MENU_LOCAL_TASK,
    );
  }
  return $items;
}


/**
 * Add the Configuration Files link to the operations
 *
 * @param $form
 * @param $form_state
 * @param $form_id
 */
function acquia_search_config_form_apachesolr_settings_alter(&$form, &$form_state, $form_id) {
  $form['apachesolr_host_settings']['table']['#header'][3]['colspan']++;
  $rows = &$form['apachesolr_host_settings']['table']['#rows'];
  foreach($rows as &$row) {
    $delete = $row['data']['delete'];
    $environment_url = $row['data']['index']['data'];
    // Find the environment ID
    preg_match('/settings\/(.*?)\/index/s', $environment_url, $matches);
    $env_id = $matches[1];
    $environment = apachesolr_environment_load($env_id);
    unset($row['data']['delete']);
    if ($environment['service_class'] == 'AcquiaSearchService') {
      $row['data']['files'] = array('class' => 'operation', 'data' => l('Config Files', format_string('admin/config/search/apachesolr/settings/!environment/files', array('!environment' => $env_id))));
    }
    else {
      $row['data']['files'] = "";
    }
    $row['data']['delete'] = $delete;
  }
}
