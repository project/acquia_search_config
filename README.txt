You can invoke the API by using the following snippet.
If you run this locally, please change the keys to the one you find in the guv_api_user table

<?php
$public_key    = "XXXXXXXX"; // Replace XXXXXXXX with your public key
$private_key   = "XXXXXXXX"; // Replace XXXXXXXX with your private key
$host = 'http://governor.acquia-search.com/api/v1/';
$path = 'index';
$url = $host . $path;
$response = acquia_search_api_signed_request($public_key, $private_key, $url);
header('Content-type: application/json');
print_r($response);

function acquia_search_api_signed_request($public_key, $private_key, $url, $content = "") {
  // Some parameters
  $verb = 'GET';
  $username = 'Acquia Search ' . $public_key;

      // This is the expected string to hash
  $string_to_hash = $verb . "\n" . $content . "\n" . $url;
  // Generate the hash
  $hash_generated = hash_hmac('sha256', $string_to_hash, $private_key);
  // base64_encode it so it fits in the Authorization Header
  $password =  base64_encode($hash_generated);

  $authorization_header = 'Authorization: ' . $username . ":" . $password;
  // Do request
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE );
  curl_setopt($ch, CURLOPT_HTTPHEADER, array($authorization_header));
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  $response = curl_exec($ch);
  if(curl_errno($ch)) {
    return curl_error($ch);
  }
  curl_close($ch);

  if (FALSE === $response) {
    return FALSE;
  }
  else {
    return $response;
  }
}