<?php

use Acquia\Search\API\SearchServiceClient;
use Guzzle\Http\ClientInterface;

/**
 * @file
 * Contains code specific to the Apache Solr Search Integration module.
 */

/**
 * Form builder for adding/editing a Solr environment used as a menu callback.
 */
function apachesolr_environment_configuration_files_form(array $form, array &$form_state, array $environment = array()) {

  $loaded = acquia_search_config_load();

  if (!$loaded) {
    return $form;
  }

  if (empty($environment)) {
    $environment = array();
  }
  $environment += array('env_id' => '', 'name' => '', 'url' => '', 'service_class' => '', 'conf' => array());

  $form['#environment'] = $environment;

  $subscription = acquia_agent_get_subscription();
  $acquia_key = acquia_agent_settings('acquia_key');
  $acquia_identifier = acquia_agent_settings('acquia_identifier');
  $search_core_id = substr( $environment['url'], strrpos( $environment['url'], '/' )+1 );

  try {
    $acquia_search_service = SearchServiceClient::factory(array(
      'search_identifier' => $search_core_id,
      'network_identifier' => $acquia_identifier,
      'network_key' => $acquia_key,
      'network_salt' => $subscription['derived_key_salt'],
    ));

    $stopwords = "";
    if($cached = cache_get('acquia_search_config_stopwords', 'cache'))  {
      if ($cached->expire > REQUEST_TIME) {
        $stopwords = $cached->data;
      }
    }
    if (empty($stopwords)) {
      $stopwords = $acquia_search_service->stopwords()->to_list();
      cache_set('acquia_search_config_stopwords', $stopwords, 'cache', REQUEST_TIME + 60 * 60); //1 hour
    }

    $protwords = "";
    if($cached = cache_get('acquia_search_config_protwords', 'cache'))  {
      if ($cached->expire > REQUEST_TIME) {
        $protwords = $cached->data;
      }
    }
    if (empty($protwords)) {
      $protwords = $acquia_search_service->protwords()->to_list();
      cache_set('acquia_search_config_protwords', $protwords, 'cache', REQUEST_TIME + 60 * 60); //1 hour
    }

    $synonyms = "";
    if($cached = cache_get('acquia_search_config_synonyms', 'cache'))  {
      if ($cached->expire > REQUEST_TIME) {
        $synonyms = $cached->data;
      }
    }
    if (empty($synonyms)) {
      $synonyms = $acquia_search_service->synonyms()->to_list();
      cache_set('acquia_search_config_synonyms', $synonyms, 'cache', REQUEST_TIME + 60 * 60); //1 hour
    }

    $suggestions = "";
    if($cached = cache_get('acquia_search_config_suggestions', 'cache'))  {
      if ($cached->expire > REQUEST_TIME) {
        $suggestions = $cached->data;
      }
    }
    if (empty($suggestions)) {
      $suggestions = $acquia_search_service->suggestions()->to_list();
      cache_set('acquia_search_config_suggestions', $suggestions, 'cache', REQUEST_TIME + 60 * 60); //1 hour
    }

    $form['#searchservice'] = $acquia_search_service;

    $form['stopwords'] = array(
      '#type' => 'textarea',
      '#title' => t('Stopwords'),
      '#default_value' => $stopwords,
      '#description' => t('These are the stopwords that are currently deployed for your Acquia Search Index. They will be removed from your index at document submission time.'),
    );

    $form['protwords'] = array(
      '#type' => 'textarea',
      '#title' => t('Protected Words'),
      '#default_value' => $protwords,
      '#description' => t('These are the protected words that are currently deployed for your Acquia Search Index. These will not be parsed for stemming.'),
    );

    $form['synonyms'] = array(
      '#type' => 'textarea',
      '#title' => t('Synonyms'),
      '#default_value' => $synonyms,
      '#description' => t('These are the Synonyms that are currently deployed for your Acquia Search Index'),
    );

    $form['suggestions'] = array(
      '#type' => 'textarea',
      '#title' => t('Suggestions'),
      '#default_value' => $suggestions,
      '#description' => t('These are the auto-complete suggestions that are currently deployed for your Acquia Search Index'),
    );
  }
  catch (Exception $e) {
    $error = t('Could not retrieve data from Acquia Search Service. Check if you have access to use this API. The service failed with !message', array('!message' => $e->getMessage()));
    drupal_set_message($error, 'error');
  }

  // Ensures destination is an internal URL, builds "cancel" link.
  if (isset($_GET['destination']) && !url_is_external($_GET['destination'])) {
    $destination = $_GET['destination'];
  }
  else {
    $destination = 'admin/config/search/apachesolr/settings';
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#href' => $destination,
  );
  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => $destination,
  );

  return $form;
}

function apachesolr_environment_configuration_files_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  if (!isset($form['#searchservice'])) {
    drupal_set_message(t('Could not instantiate a connection with the Acquia Search Service API. Please try again.'));
  }
}

function apachesolr_environment_configuration_files_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  /** @var Acquia\Search\API\SearchServiceClient $acquia_search_service */
  $acquia_search_service = $form['#searchservice'];

  if ($acquia_search_service instanceof ClientInterface) {

    // Check if stopwords need to be submitted
    $new_values = preg_replace("/[\n\r]/","",$values['stopwords']);
    $old_values = preg_replace("/[\n\r]/","",$form['stopwords']['#default_value']);
    if (isset($values['stopwords']) && $new_values != $old_values) {
      $stopwords = trim($values['stopwords']);
      $stopwords = explode(PHP_EOL, $stopwords);
      foreach ($stopwords as &$stopword) {
        rtrim($stopword, "\r");
      }
      try {
        $response = $acquia_search_service->createStopwords($stopwords);
        cache_clear_all('acquia_search_config_stopwords', 'cache');
        drupal_set_message($response->message());
      } catch (Exception $e) {
        drupal_set_message($e->getMessage(), 'error');
      }
    }

    // Check if protwords need to be submitted
    $new_values = preg_replace("/[\n\r]/","",$values['protwords']);
    $old_values = preg_replace("/[\n\r]/","",$form['protwords']['#default_value']);
    if (isset($values['protwords']) && $new_values != $old_values) {
      $protwords = trim($values['protwords']);
      $protwords = explode(PHP_EOL, $protwords);
      foreach ($protwords as &$protword) {
        rtrim($protword, "\r");
      }
      try {
        $response = $acquia_search_service->createProtwords($protwords);
        cache_clear_all('acquia_search_config_protwords', 'cache');
        drupal_set_message($response->message());
      } catch (Exception $e) {
        drupal_set_message($e->getMessage(), 'error');
      }
    }

    // Check if synonyms need to be submitted
    $new_values = preg_replace("/[\n\r]/","",$values['synonyms']);
    $old_values = preg_replace("/[\n\r]/","",$form['synonyms']['#default_value']);
    if (isset($values['synonyms']) && $new_values != $old_values) {
      $synonyms = trim($values['synonyms']);
      $synonyms = explode(PHP_EOL, $synonyms);
      foreach ($synonyms as &$synonym) {
        rtrim($synonym, "\r");
      }
      try {
        $response = $acquia_search_service->createSynonyms($synonyms);
        cache_clear_all('acquia_search_config_synonyms', 'cache');
        drupal_set_message($response->message());
      } catch (Exception $e) {
        drupal_set_message($e->getMessage(), 'error');
      }
    }

    // Check if suggestions need to be submitted
    $new_values = preg_replace("/[\n\r]/","",$values['suggestions']);
    $old_values = preg_replace("/[\n\r]/","",$form['suggestions']['#default_value']);
    if (isset($values['suggestions']) && $new_values != $old_values) {
      $suggestions = trim($values['suggestions']);
      $suggestions = explode(PHP_EOL, $suggestions);
      foreach ($suggestions as &$suggestion) {
        rtrim($suggestion, "\r");
      }
      try {
        $response = $acquia_search_service->createSuggestions($suggestions);
        cache_clear_all('acquia_search_config_suggestions', 'cache');
        drupal_set_message($response->message());
      } catch (Exception $e) {
        drupal_set_message($e->getMessage(), 'error');
      }
    }
  }
}